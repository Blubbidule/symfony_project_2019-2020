<?php

namespace App\Repository;

use App\Entity\ListeLivresSouhaites;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ListeLivresSouhaites|null find($id, $lockMode = null, $lockVersion = null)
 * @method ListeLivresSouhaites|null findOneBy(array $criteria, array $orderBy = null)
 * @method ListeLivresSouhaites[]    findAll()
 * @method ListeLivresSouhaites[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ListeLivresSouhaitesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ListeLivresSouhaites::class);
    }

    // /**
    //  * @return ListeLivresSouhaites[] Returns an array of ListeLivresSouhaites objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ListeLivresSouhaites
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
