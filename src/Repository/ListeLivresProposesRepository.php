<?php

namespace App\Repository;

use App\Entity\ListeLivresProposes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ListeLivresProposes|null find($id, $lockMode = null, $lockVersion = null)
 * @method ListeLivresProposes|null findOneBy(array $criteria, array $orderBy = null)
 * @method ListeLivresProposes[]    findAll()
 * @method ListeLivresProposes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ListeLivresProposesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ListeLivresProposes::class);
    }

    // /**
    //  * @return ListeLivresProposes[] Returns an array of ListeLivresProposes objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ListeLivresProposes
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
