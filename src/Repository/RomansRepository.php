<?php

namespace App\Repository;

use App\Entity\Romans;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Romans|null find($id, $lockMode = null, $lockVersion = null)
 * @method Romans|null findOneBy(array $criteria, array $orderBy = null)
 * @method Romans[]    findAll()
 * @method Romans[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RomansRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Romans::class);
    }

    /**
     * @return Romans[] Returns an array of Romans objects
     */
    public function findLastFourBooks() {
        return $this->createQueryBuilder('r')
            ->orderBy('r.id', 'DESC')
            ->setMaxResults(4)
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return Romans[] Returns an array of Romans objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Romans
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
