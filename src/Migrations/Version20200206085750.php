<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200206085750 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE liste_livres_proposes_romans (liste_livres_proposes_id INT NOT NULL, romans_id INT NOT NULL, INDEX IDX_68D9B945F30E46 (liste_livres_proposes_id), INDEX IDX_68D9B9DED6664C (romans_id), PRIMARY KEY(liste_livres_proposes_id, romans_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE liste_livres_souhaites_romans (liste_livres_souhaites_id INT NOT NULL, romans_id INT NOT NULL, INDEX IDX_AB95018EBCED6254 (liste_livres_souhaites_id), INDEX IDX_AB95018EDED6664C (romans_id), PRIMARY KEY(liste_livres_souhaites_id, romans_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE liste_livres_proposes_romans ADD CONSTRAINT FK_68D9B945F30E46 FOREIGN KEY (liste_livres_proposes_id) REFERENCES liste_livres_proposes (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE liste_livres_proposes_romans ADD CONSTRAINT FK_68D9B9DED6664C FOREIGN KEY (romans_id) REFERENCES romans (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE liste_livres_souhaites_romans ADD CONSTRAINT FK_AB95018EBCED6254 FOREIGN KEY (liste_livres_souhaites_id) REFERENCES liste_livres_souhaites (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE liste_livres_souhaites_romans ADD CONSTRAINT FK_AB95018EDED6664C FOREIGN KEY (romans_id) REFERENCES romans (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE commentaires ADD user_id_id INT NOT NULL, ADD book_id_id INT NOT NULL, ADD created_at DATETIME NOT NULL, DROP post_data, DROP id_utilisateur, DROP id_book');
        $this->addSql('ALTER TABLE commentaires ADD CONSTRAINT FK_D9BEC0C49D86650F FOREIGN KEY (user_id_id) REFERENCES utilisateurs (id)');
        $this->addSql('ALTER TABLE commentaires ADD CONSTRAINT FK_D9BEC0C471868B2E FOREIGN KEY (book_id_id) REFERENCES romans (id)');
        $this->addSql('CREATE INDEX IDX_D9BEC0C49D86650F ON commentaires (user_id_id)');
        $this->addSql('CREATE INDEX IDX_D9BEC0C471868B2E ON commentaires (book_id_id)');
        $this->addSql('ALTER TABLE utilisateurs ADD username VARCHAR(255) NOT NULL, ADD password VARCHAR(255) NOT NULL, ADD roles JSON NOT NULL, DROP statut, CHANGE nom nom VARCHAR(255) NOT NULL, CHANGE prenom prenom VARCHAR(255) NOT NULL, CHANGE rue rue VARCHAR(255) NOT NULL, CHANGE ville ville VARCHAR(255) NOT NULL, CHANGE email email VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE liste_livres_proposes DROP titre, DROP breve_description, DROP img_livre, CHANGE id_utilisateur searching_user_id INT NOT NULL');
        $this->addSql('ALTER TABLE liste_livres_proposes ADD CONSTRAINT FK_4FABE47BBACBECCC FOREIGN KEY (searching_user_id) REFERENCES utilisateurs (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_4FABE47BBACBECCC ON liste_livres_proposes (searching_user_id)');
        $this->addSql('ALTER TABLE liste_livres_souhaites ADD wishing_user_id INT NOT NULL');
        $this->addSql('ALTER TABLE liste_livres_souhaites ADD CONSTRAINT FK_AF66E2182BD43116 FOREIGN KEY (wishing_user_id) REFERENCES utilisateurs (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_AF66E2182BD43116 ON liste_livres_souhaites (wishing_user_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE liste_livres_proposes_romans');
        $this->addSql('DROP TABLE liste_livres_souhaites_romans');
        $this->addSql('ALTER TABLE commentaires DROP FOREIGN KEY FK_D9BEC0C49D86650F');
        $this->addSql('ALTER TABLE commentaires DROP FOREIGN KEY FK_D9BEC0C471868B2E');
        $this->addSql('DROP INDEX IDX_D9BEC0C49D86650F ON commentaires');
        $this->addSql('DROP INDEX IDX_D9BEC0C471868B2E ON commentaires');
        $this->addSql('ALTER TABLE commentaires ADD post_data DATE NOT NULL, ADD id_utilisateur INT NOT NULL, ADD id_book INT NOT NULL, DROP user_id_id, DROP book_id_id, DROP created_at');
        $this->addSql('ALTER TABLE liste_livres_proposes DROP FOREIGN KEY FK_4FABE47BBACBECCC');
        $this->addSql('DROP INDEX UNIQ_4FABE47BBACBECCC ON liste_livres_proposes');
        $this->addSql('ALTER TABLE liste_livres_proposes ADD titre VARCHAR(150) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, ADD breve_description VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, ADD img_livre VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE searching_user_id id_utilisateur INT NOT NULL');
        $this->addSql('ALTER TABLE liste_livres_souhaites DROP FOREIGN KEY FK_AF66E2182BD43116');
        $this->addSql('DROP INDEX UNIQ_AF66E2182BD43116 ON liste_livres_souhaites');
        $this->addSql('ALTER TABLE liste_livres_souhaites DROP wishing_user_id');
        $this->addSql('ALTER TABLE utilisateurs ADD statut JSON CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:json_array)\', DROP username, DROP password, DROP roles, CHANGE nom nom VARCHAR(150) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE prenom prenom VARCHAR(150) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE rue rue VARCHAR(150) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE ville ville VARCHAR(5) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE email email VARCHAR(150) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
