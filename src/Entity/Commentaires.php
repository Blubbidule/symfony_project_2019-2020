<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="App\Repository\CommentairesRepository")
 */
class Commentaires
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\Column(type="text")
     */
    private $texte;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Utilisateurs", inversedBy="commentariesId")
     * @ORM\JoinColumn(nullable=false)
     */
    private $userId;


    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Romans", inversedBy="commentairesId")
     * @ORM\JoinColumn(nullable=false)
     */
    private $bookId;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTexte(): ?string
    {
        return $this->texte;
    }

    public function setTexte(string $texte): self
    {
        $this->texte = $texte;

        return $this;
    }

    public function getUserId(): ?Utilisateurs
    {
        return $this->userId;
    }

    public function setUserId(?Utilisateurs $userId): self
    {
        $this->userId = $userId;

        return $this;
    }



    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getBookId(): ?Romans
    {
        return $this->bookId;
    }

    public function setBookId(?Romans $bookId): self
    {
        $this->bookId = $bookId;

        return $this;
    }




}
