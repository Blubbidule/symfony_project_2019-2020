<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ListeLivresProposesRepository")
 */
class ListeLivresProposes
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Utilisateurs", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $searchingUser;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Romans")
     */
    private $bookSearched;

    public function __construct()
    {
        $this->bookSearched = new ArrayCollection();
    }



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSearchingUser(): ?Utilisateurs
    {
        return $this->searchingUser;
    }

    public function setSearchingUser(Utilisateurs $searchingUser): self
    {
        $this->searchingUser = $searchingUser;

        return $this;
    }

    /**
     * @return Collection|Romans[]
     */
    public function getBookSearched(): Collection
    {
        return $this->bookSearched;
    }

    public function addBookSearched(Romans $bookSearched): self
    {
        if (!$this->bookSearched->contains($bookSearched)) {
            $this->bookSearched[] = $bookSearched;
        }

        return $this;
    }

    public function removeBookSearched(Romans $bookSearched): self
    {
        if ($this->bookSearched->contains($bookSearched)) {
            $this->bookSearched->removeElement($bookSearched);
        }

        return $this;
    }


}
