<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ListeLivresSouhaitesRepository")
 */
class ListeLivresSouhaites
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Utilisateurs", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $wishingUser;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Romans")
     */
    private $booksWished;

    public function __construct()
    {
        $this->booksWished = new ArrayCollection();
    }




    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWishingUser(): ?Utilisateurs
    {
        return $this->wishingUser;
    }

    public function setWishingUser(Utilisateurs $wishingUser): self
    {
        $this->wishingUser = $wishingUser;

        return $this;
    }

    /**
     * @return Collection|Romans[]
     */
    public function getBooksWished(): Collection
    {
        return $this->booksWished;
    }

    public function addBooksWished(Romans $booksWished): self
    {
        if (!$this->booksWished->contains($booksWished)) {
            $this->booksWished[] = $booksWished;
        }

        return $this;
    }

    public function removeBooksWished(Romans $booksWished): self
    {
        if ($this->booksWished->contains($booksWished)) {
            $this->booksWished->removeElement($booksWished);
        }

        return $this;
    }



}
