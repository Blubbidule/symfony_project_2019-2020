<?php

//App/Entity/Romans
namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RomansRepository")
 */
class Romans
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $titre;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $auteur;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $maison_edition;

    /**
     * @ORM\Column(type="string", length=4)
     */
    private $annee_edition;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $genre;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $sous_genre;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $breve_description;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $imgLivre;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $imgAuteur;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $imgQuatrCouv;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Commentaires", mappedBy="bookId", orphanRemoval=true)
     */
    private $commentairesId;


    public function __construct()
    {
        $this->commentairesId = new ArrayCollection();
    }



    public function getId(): ?int
    {
        return $this->id;
    }


    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getAuteur(): ?string
    {
        return $this->auteur;
    }

    public function setAuteur(string $auteur): self
    {
        $this->auteur = $auteur;

        return $this;
    }

    public function getMaisonEdition(): ?string
    {
        return $this->maison_edition;
    }

    public function setMaisonEdition(string $maison_edition): self
    {
        $this->maison_edition = $maison_edition;

        return $this;
    }

    public function getAnneeEdition(): ?string
    {
        return $this->annee_edition;
    }

    public function setAnneeEdition(string $annee_edition): self
    {
        $this->annee_edition = $annee_edition;

        return $this;
    }

    public function getGenre(): ?string
    {
        return $this->genre;
    }

    public function setGenre(string $genre): self
    {
        $this->genre = $genre;

        return $this;
    }

    public function getSousGenre(): ?string
    {
        return $this->sous_genre;
    }

    public function setSousGenre(string $sous_genre): self
    {
        $this->sous_genre = $sous_genre;

        return $this;
    }

    public function getBreveDescription(): ?string
    {
        return $this->breve_description;
    }

    public function setBreveDescription(string $breve_description): self
    {
        $this->breve_description = $breve_description;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getImgLivre(): ?string
    {
        return $this->imgLivre;
    }

    public function setImgLivre(string $imgLivre): self
    {
        $this->imgLivre = $imgLivre;

        return $this;
    }

    public function getImgAuteur(): ?string
    {
        return $this->imgAuteur;
    }

    public function setImgAuteur(string $imgAuteur): self
    {
        $this->imgAuteur = $imgAuteur;

        return $this;
    }

    public function getImgQuatrCouv(): ?string
    {
        return $this->imgQuatrCouv;
    }

    public function setImgQuatrCouv(string $imgQuatrCouv): self
    {
        $this->imgQuatrCouv = $imgQuatrCouv;

        return $this;
    }

    /**
     * @return Collection|Commentaires[]
     */
    public function getCommentairesId(): Collection
    {
        return $this->commentairesId;
    }

    public function addCommentairesId(Commentaires $commentairesId): self
    {
        if (!$this->commentairesId->contains($commentairesId)) {
            $this->commentairesId[] = $commentairesId;
            $commentairesId->setBookId($this);
        }

        return $this;
    }

    public function removeCommentairesId(Commentaires $commentairesId): self
    {
        if ($this->commentairesId->contains($commentairesId)) {
            $this->commentairesId->removeElement($commentairesId);
            // set the owning side to null (unless already changed)
            if ($commentairesId->getBookId() === $this) {
                $commentairesId->setBookId(null);
            }
        }

        return $this;
    }




}
