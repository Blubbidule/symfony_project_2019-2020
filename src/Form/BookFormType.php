<?php

//src/App/form/BookFormType
namespace App\Form;

use App\Entity\Romans;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class BookFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre')
            ->add('auteur')
            ->add('maison_edition')
            ->add('annee_edition')
            ->add('genre')
            ->add('sous_genre')
            ->add('breve_description')
            ->add('description')
            ->add('imgLivreFichier', FileType::class, [
                'label' => 'Image de la couverture du livre (jpg, png, jpeg)',
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',

                        'mimeTypes' => [
                            'img/jpg',
                            'img/jpeg',
                            'img/png'
                        ],
                        'mimeTypesMessage' => 'L\'image n\'a pas le bon format ou dépasse la taille autorisée',
                    ])
                ]
            ])
            ->add('imgAuteurFichier', FileType::class, [
                'label' => 'Image de l\'auteur du livre (jpg, png, jpeg)',
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'images/jpg',
                            'images/jpeg',
                            'images/png'
                        ],
                        'mimeTypesMessage' => 'L\'image n\'a pas le bon format ou dépasse la taille autorisée',
                    ])
                ]
            ])
            ->add('imgQuatrCouvFichier', FileType::class, [
                'label' => 'Image de la quatrième de couverture du livre (jpg, png, jpeg)',
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'images/jpg',
                            'images/jpeg',
                            'images/png'
                        ],
                        'mimeTypesMessage' => 'L\'image n\'a pas le bon format ou dépasse la taille autorisée',
                    ])
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Romans::class,
        ]);
    }
}
