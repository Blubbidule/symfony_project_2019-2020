<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\ListeLivresSouhaites;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;

class WishListController extends Controller
{
    /**
     * @Route("/wishList", name="wish_list")
     */
    public function createCommentary(): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        $wishBook = new ListeLivresSouhaites();
        $wishBook->setTitre('');
        $wishBook->setAuteur('');
        $wishBook->setBreveDescription('');
        $wishBook->setImgLivre('');

        $entityManager->persist($wishBook);

        $entityManager->flush();

        return new Response('Saved new wished book with id '.$wishBook->getId());
    }
}
