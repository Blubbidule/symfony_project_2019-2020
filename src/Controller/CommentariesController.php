<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Commentaires;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;

class CommentariesController extends Controller
{
    /**
     * @Route("/commentaries", name="create_commentaries")
     */
    public function createCommentary(): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        $commentary = new Commentaires();
        $commentary->setPostData('');
        $commentary->setTexte('');

        $entityManager->persist($commentary);

        $entityManager->flush();

        return new Response('Saved new commentary with id '.$commentary->getId());
    }
}
