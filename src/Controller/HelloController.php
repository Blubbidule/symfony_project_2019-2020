<?php
namespace App\Controller;

use App\Form\CommentFormType;
use App\Form\RegistrationType;
use App\Repository\CommentairesRepository;
use App\Repository\ListeLivresProposesRepository;
use App\Repository\ListeLivresSouhaitesRepository;
use App\Repository\RomansRepository;
use App\Repository\UtilisateursRepository;
use Doctrine\ORM\Mapping\Id;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use App\Entity\Romans;
use App\Entity\Commentaires;
use App\Entity\Utilisateurs;
use App\Entity\ListeLivresSouhaites;
use App\Entity\ListeLivresProposes;

class HelloController extends AbstractController
{
    /**
     * page d'accueil
     * @Route("/", name="homePage")
     */
    public function home(RomansRepository $repo) {

        $indexBooks = $repo->findLastFourBooks();
        return $this -> render('pages/index.html.twig', [
            'pageTitle' => "Accueil",
            'indexBooks' => $indexBooks
        ]);
    }

    /**
     * page de connexion
     * @Route("/connection", name="connectionPage")
     */
    public function connection(){
        return $this -> render('security/login.html.twig', [
            'pageTitle' => "Connexion"
        ]);
    }

    /**
     * page d'inscription
     * @Route("/registration", name="registrationPage")
     */
    public function registration(Request $request, ObjectManager $manager, UserPasswordEncoderInterface $encoder){

        $utilisateurs = new Utilisateurs();
        $utilisateurs->setAvatar('user_icon.png');
        $userRegistrationForm = $this->createFormBuilder($utilisateurs)
            ->add('nom')
            ->add('prenom')
            ->add('rue')
            ->add('cp')
            ->add('ville')
            ->add('phone')
            ->add('email', EmailType::class)
            ->add('username')
            ->add('password', PasswordType::class)
            ->getForm();

        $userRegistrationForm->handleRequest($request);

        if($userRegistrationForm->isSubmitted() && $userRegistrationForm->isValid()){
            $hash = $encoder->encodePassword($utilisateurs, $utilisateurs->getPassword());
            $utilisateurs->setPassword($hash);

            $manager->persist($utilisateurs);
            $manager->flush();

            return $this->redirectToRoute('connectionPage');
        }

        return $this -> render('pages/inscription.html.twig', [
            'pageTitle' => "Inscription",
            'registrationForm' => $userRegistrationForm->createView()
        ]);
    }


    /**
     * page de liste de recherche des livres
     * @Route("/bookSearchList", name="bookSearchListPage")
     */
    public function bookSearchList(RomansRepository $repo){

        $searchListBooks = $repo->findAll();
        return $this -> render('pages/liste_romans_recherche.html.twig', [
            'pageTitle' => "Liste de recherche des romans",
            'searchListBooks' => $searchListBooks
        ]);
    }

    /**
     * page de description d'un livre
     * @Route("/bookDescription/{id}", name="bookDescriptionPage")
     */
    public function bookDescription( Romans $getBookDesc, CommentairesRepository $commentsContent, Request $request, ObjectManager $manager){


        $comment = new Commentaires();
        $showAllCommentaries = $commentsContent->findAll();
        $commentForm = $this->createForm(CommentFormType::class, $comment);
        $commentForm->handleRequest($request);

        if($commentForm->isSubmitted() && $commentForm->isValid()){

            $comment->setCreatedAt(new \DateTime());
            $comment->setBookId($getBookDesc);

            $manager->persist($comment);
            $manager->flush();
            return $this->redirectToRoute('bookDescriptionPage', ['id' =>$getBookDesc->getId()]);

        }

        return $this -> render('pages/description_livre.html.twig', [
            'pageTitle' => "Description romans",
            'commentariesData' => $showAllCommentaries,
            'bookInfo' => $getBookDesc,
            'commentariesForm' => $commentForm->createView()

        ]);
    }

    /**
     * page de liste des membres
     * @Route("/usersList", name="usersListPage")
     */
    public function usersList(UtilisateursRepository $userList){

        $allUsers = $userList->findAll();
        return $this -> render('pages/liste_membres.html.twig', [
            'pageTitle' => "Liste membres",
            'userList' => $allUsers
        ]);
    }

    /**
     * page de contact
     * @Route("/contact", name="contactPage")
     */
    public function contact(){
        return $this -> render('pages/contact.html.twig', [
            'pageTitle' => "Contact"
        ]);
    }



    /**
     * page de profil
     * @Route("/userProfile/{id}", name="userProfilePage")
     */
    public function profile(Utilisateurs $userProfile){


        return $this -> render('pages/profile.html.twig', [
            'pageTitle' => "Profil",
            'userData' => $userProfile

        ]);
    }

    /**
     * page de gestion de la liste de souhaits
     * @Route("/wishListManagement", name="wishListManagementPage")
     */
    public function wishListManagement(){
        return $this -> render('pages/gestion_liste_souhait.html.twig', [
            'pageTitle' => "Gestion administrateur"
        ]);
    }

    /**
     * page de gestion de la liste de propositions
     * @Route("/giveListManagement", name="giveListManagementPage")
     */
    public function giveListManagement(){
        return $this -> render('pages/gestion_liste_proposition.html.twig', [
            'pageTitle' => "Gestion administrateur"
        ]);
    }



}