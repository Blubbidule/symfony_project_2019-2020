<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\ListeLivresProposes;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;

class SearchListController extends Controller
{
    /**
     * @Route("/searchList", name="create_search_book")
     */
    public function createSearchBook(): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        $searchBook = new ListeLivresProposes();
        $searchBook->setTitre('');
        $searchBook->setAuteur('');
        $searchBook->setBreveDescription('');
        $searchBook->setImgLivre('');

        $entityManager->persist($searchBook);

        $entityManager->flush();

        return new Response('Saved new searched book with id '.$searchBook->getId());
    }
}
