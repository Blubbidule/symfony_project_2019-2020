<?php

//src/App/Controller/FormController
namespace App\Controller;

use App\Entity\Romans;
use App\Entity\Commentaires;
use App\Entity\Utilisateurs;
use App\Form\CommentFormType;
use App\Form\UsersFormType;
use App\Repository\UtilisateursRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\ORM\Mapping\Id;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Form\BookFormType;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FormController extends Controller
{
    /**
     * Formulaire pour créer et modifier les romans
     * @Route("/booksCreate", name="createBookPage", methods="GET|POST")
     * @Route("/booksEdit/{id}", name="editBookPage", methods="GET|POST")
     */
    public function bookCreate(Romans $books = null, Request $request, ObjectManager $manager){


        $bookForm = $this->createForm(BookFormType::class, $books);
        $bookForm->handleRequest($request);

        if ($bookForm->isSubmitted() && $bookForm->isValid()) {
            //Pour l'image de couverture
            /** @var UploadedFile $bookCouvFile */
            $bookCouvFile = $bookForm->get('image de couverture')->getData();


            if ($bookCouvFile) {
                $originalFilename = pathinfo($bookCouvFile->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$bookCouvFile->guessExtension();


                try {
                    $bookCouvFile->move(
                        $this->getParameter('images'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    return ('Transfert échoué');
                }


                $books->setImgLivre($newFilename);
            }

            // ... persist the $product variable or any other work

           // return $this->redirect($this->generateUrl('image_couverture'));

            //Pour l'image de l'auteur
            /** @var UploadedFile $bookAuthorImgFile */
            $bookAuthorImgFile = $bookForm->get('image de l\'auteur')->getData();


            if ($bookAuthorImgFile) {
                $originalFilename = pathinfo($bookAuthorImgFile->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$bookAuthorImgFile->guessExtension();


                try {
                    $bookAuthorImgFile->move(
                        $this->getParameter('images'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    return ('Transfert échoué');
                }


                $books->setImgAuteur($newFilename);
            }

            // ... persist the $product variable or any other work

            // return $this->redirect($this->generateUrl('image_auteur'));

            //Pour l'image de quatrième de couverture
            /** @var UploadedFile $bookQuatrCouvImgFile */
            $bookQuatrCouvImgFile = $bookForm->get('image de la quatrième de couverture')->getData();


            if ($bookQuatrCouvImgFile) {
                $originalFilename = pathinfo($bookQuatrCouvImgFile->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$bookQuatrCouvImgFile->guessExtension();


                try {
                    $bookQuatrCouvImgFile->move(
                        $this->getParameter('images'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    return ('Transfert échoué');
                }


                $books->setImgQuatrCouv($newFilename);
            }

            // ... persist the $product variable or any other work

            // return $this->redirect($this->generateUrl('image_auteur'));


            $manager->persist($books);
            $manager->flush();
            return $this->redirectToRoute('bookDescriptionPage', ['id' =>$books->getId()]);
        }

        return $this -> render('pages/formulaire_romans.html.twig', [
            'pageTitle' => "Ajout Roman",
            'formBook' => $bookForm->createView()
        ]);


    }


    /**
     * Formulaires pour modifier ou créer des commentaires
     * @Route("/commentEdit/{id}", name="editCommentPage", methods="GET|POST")
     * @Route("/commentCreate", name="createCommentPage", methods="GET|POST")
     */
    public function commentEdit(Commentaires $comment,  Request $request, ObjectManager $manager, Romans $getBookDesc, Utilisateurs $user){

        $commentForm = $this->createForm(CommentFormType::class, $comment);

        $commentForm->handleRequest($request);
        $comment->setUserId($user);
        $comment->setBookId($getBookDesc);
        if($commentForm->isSubmitted() && $commentForm->isValid()){
            if(!$comment->getId()){
                $comment->setCreatedAt(new \DateTime());
            }
            $manager->persist($comment);
            $manager->flush();
            return $this->redirectToRoute('bookDescriptionPage', ['id' =>$getBookDesc->getId()]);
        }

        return $this->render('pages/comment_edit.html.twig', [
            'pageTitle' => "Modification commentaire",
            'commentInfo' => $comment,
            'formComment' => $commentForm->createView()
        ]);
    }

    /**
     * Page de suppression d'un commentaire
     * @Route("/commentDelete/{id}", name="deleteCommentPage")
     */
    public function deleteComment($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $comment = $entityManager->getRepository(Commentaires::class)->find($id);

        if (!$comment) {
            throw $this->createNotFoundException(
                'No user found for id '.$id
            );
        }

        $entityManager->remove($comment);
        $entityManager->flush();

        return new Response('Deleted user with id '.$comment->getId());
    }


    /**
     * Page de modification d'un utilisateur
     * @Route("/userEdit/{id}", name="editUserPage", methods="GET|POST")
     * @Route("/userCreate", name="createUserPage", methods="GET|POST")
     */
    public function userEdit(Utilisateurs $users,  Request $request, ObjectManager $manager){

        $userForm = $this->createForm(UsersFormType::class, $users);
        $userForm->handleRequest($request);
        $users->setAvatar('user_icon.png');
        $users->setRoles([]);
        if($userForm->isSubmitted() && $userForm->isValid()){

            $manager->persist($users);
            $manager->flush();
            return $this->redirectToRoute('userProfilePage', ['id' =>$users->getId()]);
        }

        return $this->render('pages/user_edit.html.twig', [
            'pageTitle' => "Modification commentaire",
            'userInfo' => $users,
            'formUser' => $userForm->createView()
        ]);
    }

    /**
     * Page de suppression d'un utilisateur
     * @Route("/userDelete/{id}", name="deleteUserPage")
     */
    public function deleteUser($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $user = $entityManager->getRepository(Utilisateurs::class)->find($id);

        if (!$user) {
            throw $this->createNotFoundException(
                'No user found for id '.$id
            );
        }

        $entityManager->remove($user);
        $entityManager->flush();

        return new Response('Deleted user with id '.$user->getId());
    }





}
