<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Utilisateurs;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;

class UsersController extends Controller
{
    /**
     * @Route("/users", name="users")
     */
    public function createCommentary(): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        $users = new Utilisateurs();
        $users->setVille('');
        $users->setNom('');
        $users->setPrenom('');
        $users->setEmail('');
        $users->setPhone('');
        $users->setRue('');
        $users->setCp('');

        $entityManager->persist($users);

        $entityManager->flush();

        return new Response('Saved new user with id '.$users->getId());
    }
}
