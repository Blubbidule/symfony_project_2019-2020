<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Romans;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class BooksController extends Controller
{
    /**
     * @Route("/books", name="createBooks")
     */
    public function createBook(): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        $book = new Romans();
        $book->setTitre('');
        $book->setAuteur('');
        $book->setMaisonEdition('');
        $book->setAnneeEdition('');
        $book->setGenre('');
        $book->setSousGenre('');
        $book->setBreveDescription('');
        $book->setDescription('');
        $book->setImgAuteur('');
        $book->setImgLivre('');
        $book->setImgQuatrCouv('');

        $entityManager->persist($book);

        $entityManager->flush();

       /* $errors = $validator->validate($product);
        if (count($errors) > 0) {
            return new Response((string) $errors, 400);
        }*/

        return new Response('Saved new book with id '.$book->getId());
    }


    /**
     * @Route("/books/delete/{id}", name="deleteBook")
     */
    public function delete($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $book = $entityManager->getRepository(Romans::class)->find($id);

        if (!$book) {
            throw $this->createNotFoundException(
                'No book found for id '.$id
            );
        }

        $entityManager->remove($book);
        $entityManager->flush();

        return new Response('Deleted book with id '.$book->getId());
    }

}
