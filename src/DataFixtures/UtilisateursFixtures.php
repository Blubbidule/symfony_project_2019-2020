<?php

namespace App\DataFixtures;

use App\Entity\Utilisateurs;
use App\Entity\Romans;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UtilisateursFixtures extends Fixture
{
    private $passwordEncoder;
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
     {
       $this->passwordEncoder = $passwordEncoder;
     }

    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i <= 4; $i++){
            $user = new Utilisateurs();
            $user->setPrenom('Jean');
            $user->setNom('Michel');
            $user->setCp('4100');
            $user->setRue('Rue des bouftous');
            $user->setVille('Liège');
            $user->setPhone('0445782635');
            $user->setEmail('jeanmichel@gmail.Com');
            $user->setUsername('jeanmichel'.$i);
            $user->setAvatar('user_icon.png');
            $user->getRoles();
            $user->setPassword($this->passwordEncoder->encodePassword(
                $user,
                'the_new_password'
            ));






            $manager->persist($user);
        }

        for ($i = 1; $i <= 10; $i++){
            $books = new Romans();
            $books->setTitre('Roman n°'.$i);
            $books->setAuteur('Auteur n°'.$i);
            $books->setMaisonEdition('Imprimerie n°'.$i);
            $books->setAnneeEdition('2015');
            $books->setGenre('Polar');
            $books->setSousGenre('Polar');
            $books->setBreveDescription('Lorem Ipsum');
            $books->setDescription('Renversé au fond d\'une barque juste en bas. Volontiers, répondit-il, 
                je pourrais obéir. Menton collé au torse, pour rattraper leurs petites proies peu véloces dans cet escalier 
                de bois qui s\'élevait ferme et dure. Multiples brûlures : sur le bord du trottoir, attendant comme les autres, seuls tricheurs.');
            $books->setImgLivre('couverture_1.jpeg');
            $books->setImgAuteur('laurentgounelle.jpeg');
            $books->setImgQuatrCouv('quatrieme_couverture_gounelle.jpg');

            $manager->persist($books);
        }

        $manager->flush();

    }
}
